ARG IMAGE=3.10

FROM python:${IMAGE} as SRC

ARG VENV=.py.venv
ARG WDIR=/opt/plot

WORKDIR ${WDIR}

ENV VIRTUAL_ENV=${WDIR}/${VENV}
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt




FROM python:${IMAGE}-alpine

ARG VENV=.py.venv
ARG WDIR=/opt/plot

WORKDIR ${WDIR}

RUN mkdir -p ${WDIR}/data.d \
&&  mkdir -p ${WDIR}/plot.d

COPY --from=SRC $WDIR/$VENV ./$VENV
COPY plot.py ./ 

ENV VIRTUAL_ENV=${WDIR}/${VENV}
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

CMD ["python", "version"]
