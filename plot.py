import os
import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams["figure.figsize"] = [7.50, 3.50]
plt.rcParams["figure.autolayout"] = True

headers = ['Name', 'Age', 'ID']

directory = '/opt/plot/'
typ = '.csv'

for file in [f for f in os.listdir(str(directory)+"data.d") if f.lower().endswith(typ)]:

    df = pd.read_csv('PlotCSV.csv', names=headers, sep='\t', skiprows=(1))

    df.set_index('Name').plot()

    #plt.show()
    plt.savefig(str(directory)+"plot.d"+os.path.splitext(os.path.basename(file))[0].png)
